# Introduction

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Goals](#goals)
- [Basics](#basics)
- [Concepts](#concepts)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview
The *Centros.one* project is a collection of components and utilities designed to facilitate the development and deployment
of microservice architectures using event-based communication between service instances.

## Goals
The purpose of this project is to provide a toolbox for developers of system architectures that consist of microservice
components using a common event bus for communication which is useful during the systems entire lifecycle, starting from
the design phase of the architecture itself, through development of the individual services, all the way to ultimately
the deployment of the system on the target platform.

## Basics
*Centros.one* builds upon the following existing software frameworks and concepts:

* For inter-service communication: [Apache Kafka](https://kafka.apache.org/) with events in [Apache Avro](https://avro.apache.org/)
  format to ensure messages sent between services are compatible and consistent
* For packaging and deploying service instances: [CNAB](https://cnab.io/) (**C**loud **N**ative **A**pplication **B**undles) using
  [Porter](https://porter.sh/)

## Concepts
In the scope of Centros, services are considered independent building blocks with defined connection points (called "sockets")
which are directed (i.e. either input or output) and used to read or write messages in a specific format, given as Avro schemas
associated with these sockets.

Services can then be treated as individual black boxes with the sockets (and their schema definitions) representing a kind of
*contract* guaranteeing a certain structure for the data the service can consume and produce.

An architecture then essentially is simply expressed as a list of the service instances it consists of as well as the connections
among their sockets. Automatic checking (for which *Centros.one* provides the necessary tools) can then be used as part of the
deployment process to verify that the architecture definition is valid (i.e. input and output schemas of connected sockets are
mutually compatible etc.)
