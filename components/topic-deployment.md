# Topic Deployment

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Usage](#usage)
  - [Parameters](#parameters)
  - [Credentials](#credentials)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

This is a bundle for facilitating the deployment of topics to a Kafka cluster

## Usage

```
porter install <topic-name> \
  --reference core.harbor.clouster.io/library/topic-deployment:v1.1.2 \
  -p <topic-deployment-params> \
  -c <topic-deployment-credentials>
```

### Parameters

- `kafka-cluster-name`: the name of the Kafka cluster (typically, this is the name under which the Kafka Helm chart was installed)
- `kafka-bootstrap-server`: the address of a server in the Kafka cluster for bootstrapping
- `namespace`: the kubernetes namespace in which the Kafka cluster is deployed (optional, default: `default`)
- `image-pull-secret`: the image pull secret to use for pulling the topic-deployment image (optional, default: no image-pull-secret)
- `image-pull-policy`: the image pull policy to use for pulling the topic-deployment image (optional, default: `IfNotPresent`)
- `cluster-ready-timeout`: the amount of time (in seconds) to wait for the Kafka cluster to be ready (optional, default: 600)
- `job-complete-timeout`: the amount of time (in seconds) to wait for the deployment job to complete (optional, default: 240)
- `kubectl-timeout`: the amount of time (in seconds) to wait for calls to `kubectl` to complete (optional, default: 10)
- `topic-partitions`: number of partitions for the topic (optional, default: 1)
- `topic-replicas`: number of replicas for the topic (optional, default: 1)
- `topic-config-json`: any additional configuration parameters for the topic, as a JSON string (optional, default: none)

### Credentials
- `kubeconfig`: the kubeconfig for the kubernetes cluster the secret will be created on
