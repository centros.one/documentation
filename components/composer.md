# Composer

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Input](#input)
- [Validations](#validations)
- [Generators](#generators)
  - [`default`](#default)
  - [`diagram`](#diagram)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

The Composer utility is responsible for parsing Centros Configuration Format files (.ccf), validating the validity of
the microservice architecture described within and finally producing output representing the services in a comprehensive
format (depending on the chosen generator).

## Input

Input files for the Composer are in JSON format conforming to the [CCF](../schemas/ccf.md)
schema.

They contain the list of service instances that are necessary to deploy the service architecture on a target system as well as a description
of the topics required for these services to communicate with each other.

## Validations

After reading the input file (and verifying its syntactic integrity by validating against the CCF schema) the Composer carries out
a number of checks to determine whether the architecture described within fulfills a number of criteria for semantic correctness:

1. The referenced service types/versions exist (by querying the repository for metadata relating to the service)
2. Credentials required for installing the services are either present or are considered to be provided at deploy time (e.g. kubeconfig)
3. Configuration parameters of the individual service instances match the configuration schema given by the service definition
4. Configuration of Connectors (cf. Kafka Connect) is valid
5. Several checks to validate the connectivity between sockets:
  1. Socket names are unique
  2. No sockets are unconnected (if this is violated it is not an error, Composer only log this as a warning and continues)
  3. The Input and output sockets that topics connect to exist
  4. The schemas of sockets connected by a topic are compatible

If all validations are successful, Composer proceeds to the generation of the output

## Generators

The following output generators are supported:

### `default`

This generator creates a JSON file containing all the information that should be necessary and sufficient for installing the
service architecture described by the input file on the target system:
- Image Pull Secrets
- Services
- Topics (to be created in the Kafka cluster)
- Schemas (to be registered in the Schema Registry)
- Connectors (to be created in Kafka Connect)

### `diagram`

Generates a visual representation of the service architecture showing which services connect which topics for exchanging messages
