# CSerPH

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Project structure](#project-structure)
- [Subcommands](#subcommands)
  - [`validate`](#validate)
  - [`render`](#render)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

CSerPH (**C**entros **Ser**vice **P**ackaging **H**elper) is a utility for performing a number of tasks needed during the CI build pipeline for
services.

## Project structure

CSerPH expects a service project to follow a certain structural layout: within the root folder of the project, a folder named `cnab`, containing a template
for a porter bundle, named `porter.yaml.tmpl`.

This template may contain a service description (CSM, i.e. **C**entros **S**ervice **M**etadata) containing information about the input/output sockets of the service as well as a schema for validating
the configuration parameters of a service instance.

During the build process this template will be transformed into the `porter.yaml` used to actually build the CNAB, including various build-time values
(version number, image digests etc.) and inline schema definitions for the input/output sockets of the service as described by the CSM.

## Subcommands

CSerPH currently offers 2 subcommands that can be used as part of a build step in the CI pipeline:

### `validate`

This subcommand is useful as a pre-build validation step to ensure that the service being built matches the expected structure. It performs
the following checks and returns an error code in case of a failure:

- The `cnab` subfolder exists and contains a `porter.yaml.tmpl` file
- The `porter.yaml.tmpl` file matches the [schema](https://gitlab.com/centros.one/packaging/cserph/-/blob/master/schemas/porter/0.33/porter-schema.json)
  for porter bundle configuration files
- The `porter.yaml.tmpl` contains a CSM section which conforms to the
  [CSM](../schemas/csm.md) schema
  (the CSM can be made optional with the command-line option `-n/--no-csm`)

### `render`

Transforms the template into the form required for building the porter bundle; it can be used to enhance the generated `porter.yaml` with information
that will only be known at build time (in order to avoid having to manually update and build the bundle in a second step) like the version number (given
by the git tag) of the service, or the digest of the service image that has just been built, so it can be included automatically in the final bundle
as the result of a single pipeline execution.

To this end, it expects a number of '-u/--update' command line parameters, specifying the keys in the yaml structure and the values that should be
assigned to them, as in this example:

```
cserph render
      -u /version="$CI_COMMIT_TAG"
      -u /registry="$DOCKER_REPO_PROJECT"
      -u /images/scrapy/repository="$CI_REGISTRY_IMAGE"
      -u /images/scrapy/digest="$IMAGE_DIGEST"
```

This will cause the given fields in the final `porter.yaml` to be updated to the value of the given CI variables.

Additionally, if the template contains a CSM definition, referenced schemas will be inlined into it so they are also included in the bundle (as part of
the manifest).
