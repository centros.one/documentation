# Connector Deployment

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Usage](#usage)
  - [Parameters](#parameters)
  - [Credentials](#credentials)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

This is a bundle for facilitating the deployment of Connectors to a Kafka Connect cluster

## Usage

```
porter install <connector-name> \
  --reference core.harbor.clouster.io/library/connector-deployment:v1.1.2 \
  -p <connector-deployment-params> \
  -c <connector-deployment-credentials>
```

### Parameters

- `connect-cluster-name`: the name of the Kafka Connect cluster (typically, this is the name under which the Kafka Connect Helm chart was installed)
- `connect-url`: the URL for accessing the Kafka Connect REST API
- `namespace`: the kubernetes namespace in which the Kafka Connect cluster is deployed (optional, default: `default`)
- `image-pull-secret`: the image pull secret to use for pulling the connector-deployment image (optional, default: no image-pull-secret)
- `image-pull-policy`: the image pull policy to use for pulling the connector-deployment image (optional, default: `IfNotPresent`)
- `cluster-ready-timeout`: the amount of time (in seconds) to wait for the Kafka Connect cluster to be ready (optional, default: 600)
- `job-complete-timeout`: the amount of time (in seconds) to wait for the deployment job to complete (optional, default: 240)
- `kubectl-timeout`: the amount of time (in seconds) to wait for calls to `kubectl` to complete (optional, default: 10)
- `connector-config-json`: the configuration parameters for the connector, as a JSON string (optional, default: none)

### Credentials
- `kubeconfig`: the kubeconfig for the kubernetes cluster the secret will be created on
