# Schema Deployment

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Usage](#usage)
  - [Parameters](#parameters)
  - [Credentials](#credentials)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

This is a bundle for facilitating the deployment of Schemas to a Kafka Schema Registry

## Usage

```
porter install <schema-subject> \
  --reference core.harbor.clouster.io/library/schema-deployment:v1.1.0 \
  -p <schema-deployment-params> \
  -c <schema-deployment-credentials>
```

### Parameters

Note that the ***name*** of the installation will be used as the ***subject*** on which the schema will be registered.

- `schema-registry-name`: the name of the Schema Registry (typically, this is the name under which the Schema Registry Helm chart was installed)
- `schema-definition`: the definition of the AVRO schema as a JSON string
- `namespace`: the kubernetes namespace in which the Schema Registry is deployed (optional, default: `default`)
- `registry-ready-timeout`: the amount of time (in seconds) to wait for the Schema Registry to be ready (optional, default: 600)
- `kubectl-timeout`: the amount of time (in seconds) to wait for calls to `kubectl` to complete (optional, default: 10)

### Credentials
- `kubeconfig`: the kubeconfig for the kubernetes cluster the secret will be created on
