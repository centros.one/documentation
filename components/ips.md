# Image Pull Secret

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Usage](#usage)
  - [Parameters](#parameters)
  - [Credentials](#credentials)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

This is a bundle for facilitating the creation of image pull secrets that may be required for the installation of other bundles

## Usage

```
porter install <pull-secret-name> \
  --reference core.harbor.clouster.io/library/image-pull-secret:v1.0.0 \
  -p <pull-secret-params> \
  -c <pull-secret-credentials>
```

### Parameters

- `registry`: the container registry
- `username`: the username for authentication at the given container registry
- `password`: the password for authentication at the given container registry
- `email`: the user email (optional, default: `""`)
- `namespace`: the kubernetes namespace in which the secret will be created (optional, default: `default`)

### Credentials
- `kubeconfig`: the kubeconfig for the kubernetes cluster the secret will be created on
