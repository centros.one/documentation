# CSM (Centros Service Metadata)

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Example](#example)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

The CSM schema defines the structure for describing an individual microservice component. It provides a list of the input and output
sockets, i.e. the interface the service uses to communicate with the larger system, including Avro schemas of the data structures exchanged
via this interface.

The current schema definition can be found here: https://gitlab.com/centros.one/composer/schemas/-/blob/master/csm-schema.json

The CSM for the service that it describes must be included in the definition file for the CNAB packaging the service (`porter.yaml`) as a
`custom` entry with the key `one.centros.csm`, so it is included in the service manifest as published to the repository and thus available
for validation and processing during deployment.

## Example

```
    sockets:
      input:
      - name: image-in
        keySchema:
          type: avro
          uri: ../schema/image/image_key_v1.avsc
        valueSchema:
          type: avro
          uri: ../schema/image/image_v1.avsc
      output:
      - name: object-out
        keySchema:
          type: avro
          uri: ../schema/object/object_key_v1.avsc
        valueSchema:
          type: avro
          uri: ../schema/object/object_v1.avsc
    configSchema:
      type: object
      required:
      - threshold
      properties:
        threshold:
          type: number
        logLevel:
          type: string
          enum:
          - TRACE
          - DEBUG
          - INFO
          - WARN
          - ERROR
          - FATAL
```

This example defines one input socket, named `image-in`, and one output socket `object-out` which receive/produce Kafka events with the given
key and value schemas.

The `configSchema` section additionally describes the configuration parameters for the service, one of which is required, which means this is something the 
[Composer](../components/composer.md) utility will check for (and respond to with an error if the parameter is omitted) while assembling a service architecture.