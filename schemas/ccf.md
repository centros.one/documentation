# CCF (Centros Configuration File)

**Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

The CCF schema defines the structure for describing an architecture consisting of individual service instances as well as the
Kafka topics required for the communication between the services.

The current schema definition can be found here: https://gitlab.com/centros.one/composer/schemas/-/blob/master/ccf-schema.json

Files in this format are then used by the [Composer](../components/composer.md) utility to assemble all the required information
for deploying this architecture to the target platform (or process it in some other way, e.g. generating a visual representation
of the system).
