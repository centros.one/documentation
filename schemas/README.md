# Schemas

*Centros.one* defines a number of JSON schemas for data describing aspects like indiviual microservice components or a system
architecture, which will be used for automatic validation.

These schemas (and their purposes) will be described in this section.